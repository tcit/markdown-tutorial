// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import Vuex from 'vuex';
import VuexI18n from 'vuex-i18n';
import translations from '@/lessons/lang.json';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

import App from './App';
import router from './router';

Vue.config.productionTip = false;

Vue.use(Vuex);
Vue.use(VuexI18n);

let language = window.navigator.userLanguage || window.navigator.language;

Vue.use(BootstrapVue);

if (!(language in translations)) {
  [language] = language.split('-', 1);
}

const store = new Vuex.Store();

Vue.use(VuexI18n.plugin, store);
Object.entries(translations).forEach((key) => {
  Vue.i18n.add(key[0], key[1]);
});

console.log(language);

Vue.i18n.set(language);
Vue.i18n.fallback('en');


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
});
