{
  "lessons": [
    {
      "title": "Italique et Gras",
      "introduction": "Nous commencerons par découvrir deux éléments de base utiles pour le formattage de texte : l'*italique* et le **gras**. \n\nDurant ces leçons, vous remarquerez du texte `affiché en rouge ainsi` : ce texte est écrit en Markdown ! Le langage Markdown n'apparait pas différemment du texte normal, mais nous le mettons en valeur pour qu'il soit plus visible à vos yeux.",
      "chapters": [
        {
          "title": "Italique",
          "body": "Pour afficher une phrase ou un mot en *italique* en Markdown, vous pouvez encadrer un ou plusieurs blocs de texte avec un underscore (`_`) (que vous trouverez habituellement avec la combinaison de touches <kbd>AltGr</kbd> + <kbd>8</kbd>). Par exemple, `_ce mot_` deviendrait *italique*.\n\nPour valider cette leçon, vous devez afficher le mot \"pas\" en italique",
          "question": {
            "input": "Écrire en Markdown n'est pas si compliqué !",
            "result": "Écrire en Markdown n'est _pas_ si compliqué !"
          }
        },
        {
          "title": "Gras",
          "body": "Super ! Bon travail.\n\nDe la même manière, pour afficher un texte **en gras** avec Markdown, vous pouvez encadrer des blocs de texte avec deux astérisques ( `**` ). Cela permettra d'appuyer`**vraiment**` sur certains points.\n\nDans la zone de texte ci-dessous, affichez le mot \"finirai\" en gras.",
          "question": {
            "input": "Je finirai ces leçons !",
            "result": "Je **finirai** ces leçons !!"
          }
        },
        {
          "title": "Gras et italique",
          "body": "Bon travail !\n\nBien sûr, vous pouvez utiliser à la fois `_de l'italique et du gras_` sur la même ligne. .\n\nDans la zone de texte ci-dessous, affichez les mots \"Of course\" en italique, et les mots \"a little moxie\" en gras.",
          "question": {
            "input": "\"Of course,\" she whispered. Then, she shouted: \"All I need is a little moxie!\"",
            "result": "\"*Of course*,\" she whispered. Then, she shouted: \"All I need is **a little moxie**!\""
          }
        },
        {
          "title": "Mélanger gras et italique",
          "body": "Fantastique ! Pour l'exercice final de cette leçon, nous allons rendre certains mots *en gras **et** en italique*.\n\nEn général, l'ordre des astérisques ou des underscores n'est pas important. Dans la zone de texte ci-dessous, rendez les mots \"This is unbelievable\" à la fois en gras et en italique. Placez les astérisques `**_à l'extérieur_**`, juste pour rendre le texte plus lisible.",
          "question": {
            "input": "If you're thinking to yourself, This is unbelievable, you'd probably be right.",
            "result": "If you're thinking to yourself, **_This is unbelievable_**, you'd probably be right."
          }
        }
      ],
      "conclusion": "Vous savez maintenant comment rendre du texte gras et italique en Markdown !"
    },
    {
      "title": "Headers",
      "introduction": "Let's take a look at another formatting convention: the header. Headers are frequently used on websites, magazine articles, and notices, to draw attention to a section. As their name implies, they act like titles or subtitles above sections.",
      "chapters": [
        {
          "title": "Heading level",
          "body": "There are six types of headers, in decreasing sizes:\n\n# This is header one\n## This is header two\n### This is header three\n#### This is header four\n##### This is header five\n ###### This is header six</h6>\n\nTo make headers in Markdown, you preface the phrase with a hash mark (`#`). You place the same number of hash marks as the size of the header you want. For example, for a header one, you'd use one hash mark (`# Header One`), while for a header three, you'd use three (<code>### Header Three</code>).\n\nFor this next lesson, make each header the right size.",
          "question": {
            "input": "Header one\nHeader two\nHeader three\nHeader four\nHeader five\nHeader six",
            "result": "# Header one\n## Header two\n### Header three\n#### Header four\n##### Header five\n###### Header six"
          }
        },
        {
          "title": "Choose a heading level",
          "body": "All right!\n\nIt's up to you to decide when it's appropriate to use which header. In general, headers one and six should be used sparingly.\n\nYou can't really make a header bold, but you can italicize certain words. In the box below, make the first line a heading level four, and italicize the name of the book:",
          "question": {
            "input": "Colombian Symbolism in One Hundred Years of Solitude\n\nHere's some words about the book _One Hundred Years..._.",
            "result": "#### Colombian Symbolism in _One Hundred Years of Solitude_\n\nHere's some words about the book _One Hundred Years..._."
          }
        }
      ],
      "conclusion": "And that's all there is to making headers in Markdown."
    },
    {
      "title": "Links",
      "introduction": "We'll now learn how to make links to other web sites on the world wide web.",
      "chapters": [
        {
          "title": "Inline links",
          "body": "There are two different link types in Markdown, but both of them render the exact same way. The first link style is called an *inline link*. To create an inline link, you wrap the link text in brackets ( `[ ]` ), and then you wrap the link in parenthesis ( `( )` ). For example, to create a hyperlink to https://github.com, with a link text that says, Visit GitHub!, you'd write this in Markdown: `[Visit GitHub!](https://github.com)`.\n\nIn the box below, make a link to https://google.com, with link text that says \"Search for it.\"",
          "question": {
            "input": "Search for it.",
            "result": "[Search for it.](https://google.com)"
          }
        },
        {
          "title": "Style link texts",
          "body": "Nice work!\n\nYou can add emphasis to link texts, if you like. In the box below, make the phrase \"really, really\" bold, and have the entire sentence link to http://dailykitten.com. You'll want to make sure that the bold phrasing occurs within the link text brackets.",
          "question": {
            "input": "You're really, really going to want to see this.",
            "result": "[You're **really, really** going to want to see this.](http://dailykitten.com)</p>"
          }
        },
        {
          "title": "Links in headings",
          "body": "Fantastic!\n\nAlthough it might make for an awkward experience, you can make links within headings, too. For this next tutorial, make the text a heading four, and turn the phrase \"the BBC\" into a link to https://bbc.com/news:",
          "question": {
            "input": "The Latest News from the BBC",
            "result": "#### The Latest News from [the BBC](https://bbc.com/news)"
          }
        },
        {
          "title": "Reference links",
          "body": "That's all there is to writing inline links.\n\nThe other link type is called a <em>reference</em> link. As the name implies, the link is actually a reference to another place in the document. Here's an example of what we mean:\n\n```Here's [a link to something else][another place].\nHere's [yet another link][another-link].\nAnd now back to [the first link][another place].\n[another place]: www.github.com\n[another-link]: www.google.com\n```\n\nThe \"references\" above are the second set of brackets: `[another place]` and `[another-link]`. At the bottom of a Markdown document, these brackets are defined as proper links to outside websites. An advantage of the reference link style is that multiple links to the same place only need to be updated once. For example, if we decide to make all of the `[another place]` links go somewhere else, we only have to change the single reference link.\n\nReference links don't appear in the rendered Markdown. You define them by providing the same tag name wrapped in brackets, followed by a colon, followed by the link.\n\nIn the box below, we've started writing out some reference links. You'll need to finish them up! Call the first reference tag \"a fun place\", and make it link to www.zombo.com; make the second link out to www.stumbleupon.com.",
          "question": {
            "input": "Do you want to [see something fun][]?\n\nWell, do I have [the website for you][another fun place]!",
            "result": "Do you want to [see something fun](http://www.zombo.com)?\n\nWell, do I have [the website for you](http://www.stumbleupon.com)!"
          }
        }
      ],
      "conclusion": "You now know how to make links in Markdown!"
    },
    {
      "title": "Images",
      "introduction": "If you know how to create links in Markdown, you can create images, too. The syntax is nearly the same.",
      "chapters": [
        {
          "title": "Syntax",
          "body": "Images also have two styles, just like links, and both of them render the exact same way. The difference between links and images is that images are prefaced with an exclamation point ( `!` ).\n\nThe first image style is called an *inline image link*. To create an inline image link, enter an exclamation point ( `!` ), wrap the alt text in brackets ( `[ ]` ), and then wrap the link in parenthesis ( `( )` ). (Alt text is a phrase or sentence that describes the image for the visually impaired.)\n\nFor example, to create an inline image link to https://octodex.github.com/images/bannekat.png, with an alt text that says, Benjamin Bannekat, you'd write this in Markdown: `![Benjamin Bannekat](https://octodex.github.com/images/bannekat.png)`.\n\nIn the box below, turn the link to an image, and fill out the alt text brackets to say \"A representation of Octdrey Catburn\":",
          "question": {
            "input": "[](http://octodex.github.com/images/octdrey-catburn.jpg)",
            "result": "![A representation of Octdrey Catburn](http://octodex.github.com/images/octdrey-catburn.jpg)"
          }
        },
        {
          "title": "Images and reference links",
          "body": "Wonderful!\n\nAlthough you don't *need* to add alt text, it will make your content accessible to your audience, including people who are visually impaired, use screen readers, or do not have high speed internet connections.\n\nFor a reference image, you'll follow the same pattern as a reference link. You'll precede the Markdown with an exclamation point, then provide two brackets for the alt text, and then two more for the image tag. At the bottom of your Markdown page, you'll define an image for the tag.\n\nIn the box below, we've started placing some reference images; you'll need to complete them, just like the last lesson. Call the first reference tag \"First Father\", and make it link to http://octodex.github.com/images/founding-father.jpg; make the second image link out to http://octodex.github.com/images/foundingfather_v2.png.",
          "question": {
            "input": "[The first father][]\n\n[The second first father][Second Father]",
            "result": "![The first father](http://octodex.github.com/images/founding-father.jpg)\n\n![The second first father](http://octodex.github.com/images/foundingfather_v2.png)"
          }
        }
      ],
      "conclusion": "Ta da! You've learned all there is to adding images in Markdown!"
    },
    {
      "title": "Blockquotes",
      "introduction": "If you need to call special attention to a quote from another source, or design a pull quote for a magazine article, then Markdown's <em>blockquote</em> syntax will be useful. A blockquote is a sentence or paragraph that's been specially formatted to draw attention to the reader. For example:\n>\"The sin of doing nothing is the deadliest of all the seven sins. It has been said that for evil men to accomplish their purpose it is only necessary that good men should do nothing.\"",
      "chapters": [
        {
          "title": "Syntax",
          "body": "To create a block quote, all you have to do is preface a line with the \"greater than\" caret (`>`). For example:\n```\n\n> \"In a few moments he was barefoot, his stockings folded in his pockets and his canvas shoes dangling by their knotted laces over his shoulders and, picking a pointed salt-eaten stick out of the jetsam among the rocks, he clambered down the slope of the breakwater.\"\n```\n\nIn the box below, turn the book quotation into a blockquote:",
          "question": {
            "input": "I read this interesting quote the other day:\n\n\"Her eyes had called him and his soul had leaped at the call. To live, to err, to fall, to triumph, to recreate life out of life!\"",
            "result": "I read this interesting quote the other day:\n\n> \"Her eyes had called him and his soul had leaped at the call. To live, to err, to fall, to triumph, to recreate life out of life!\""
          }
        },
        {
          "title": "Multiple paragraphs",
          "body": "Marvelous!\n\nYou can also place a caret character on each line of the quote. This is particularly useful if your quote spans multiple paragraphs. For example:\n\n```\n> His words seemed to have struck some deep chord in his own nature. Had he spoken of himself, of himself as he was or wished to be? Stephen watched his face for some moments in silence. A cold sadness was there. He had spoken of himself, of his own loneliness which he feared.\n>\n> —Of whom are you speaking? Stephen asked at length.\n>\n> Cranly did not answer.\n```\n\nNotice that even blank lines must contain the caret character. This ensures that the entire blockquote is grouped together.\n\nIn the box below, Make the entire quotation a block quote by inserting a caret on each line.",
          "question": {
            "input": "> Once upon a time and a very good time it was there was a moocow coming down along the road and this moocow that was coming down along the road met a nicens little boy named baby tuckoo...\n\nHis father told him that story: his father looked at him through a glass: he had a hairy face.\n\nHe was baby tuckoo. The moocow came down the road where Betty Byrne lived: she sold lemon platt.",
            "result": "> Once upon a time and a very good time it was there was a moocow coming down along the road and this moocow that was coming down along the road met a nicens little boy named baby tuckoo...\n>\n> His father told him that story: his father looked at him through a glass: he had a hairy face.\n>\n> He was baby tuckoo. The moocow came down the road where Betty Byrne lived: she sold lemon platt."
          }
        },
        {
          "title": "Embed other elements",
          "body": "Tremendous!\n\nBlock quotes can contain other Markdown elements, such as italics, images, or links.\n\nIn the box below, make the French text italic (not including the exclamation point).\n\nAlso, turn the entire quote into a blockquote.",
          "question": {
            "input": "He left her quickly, fearing that her intimacy might turn to jibing and wishing to be out of the way before she offered her ware to another, a tourist from England or a student of Trinity. Grafton Street, along which he walked, prolonged that moment of discouraged poverty. In the roadway at the head of the street a slab was set to the memory of Wolfe Tone and he remembered having been present with his father at its laying. He remembered with bitterness that scene of tawdry tribute. There were four French delegates in a brake and one, a plump smiling young man, held, wedged on a stick, a card on which were printed the words: VIVE L'IRLANDE!",
            "result": "> He left her quickly, fearing that her intimacy might turn to jibing and wishing to be out of the way before she offered her ware to another, a tourist from England or a student of Trinity. Grafton Street, along which he walked, prolonged that moment of discouraged poverty. In the roadway at the head of the street a slab was set to the memory of Wolfe Tone and he remembered having been present with his father at its laying. He remembered with bitterness that scene of tawdry tribute. There were four French delegates in a brake and one, a plump smiling young man, held, wedged on a stick, a card on which were printed the words: *VIVE L'IRLANDE*!"
          }
        }
      ],
      "conclusion": "Ta da! You've learned all there is to creating blockquotes in Markdown!"
    },
    {
      "title": "Lists",
      "introduction": "This tutorial is all about creating lists in Markdown.",
      "chapters": [
        {
          "title": "Syntax",
          "body": "There are two types of lists in the known universe: unordered and ordered. That's a fancy way of saying that there are lists with bullet points, and lists with numbers.\n\nTo create an unordered list, you'll want to preface each item in the list with an asterisk ( <code>*</code> ). Each list item also gets its own line. For example, a grocery list in Markdown might look like this:\n\n```\n* Milk\n* Eggs\n* Salmon\n* Butter\n```\n\nThis Markdown list would render into the following bullet points:\n\n* Milk\n* Eggs\n* Salmon\n* Butter\n",
          "question": {
            "input": "Flour, Cheese, Tomatoes",
            "result": "* Flour\n* Cheese,\n* Tomatoes"
          }
        },
        {
          "title": "Numbered lists",
          "body": "All right! That's how you write an unordered list. Now, let's talk about ordered ones.\n\nAn ordered list is prefaced with numbers, instead of asterisks. Take a look at this recipe:\n\n1. Crack three eggs over a bowl\n2. Pour a gallon of milk into the bowl\n3. Rub the salmon vigorously with butter\n4. Drop the salmon into the egg-milk bowl\n\nTo write that in Markdown, you'd do this:\n\n```1. Crack three eggs over a bowl\n2. Pour a gallon of milk into the bowl\n3. Rub the salmon vigorously with butter\n4. Drop the salmon into the egg-milk bowl\n```\n\nEasy, right? It's just like you'd expect a list to look.\n\nIn the box below, turn the rest of the recipe into an ordered list.",
          "question": {
            "input": "Cut the cheese, Slice the tomatoes, Rub the tomatoes in flour",
            "result": "1. Cut the cheese,\n2. Slice the tomatoes,\n3. Rub the tomatoes in flour"
          }
        },
        {
          "title": "Stylize lists",
          "body": "Fantastic work!\nYou can choose to add italics, bold, or links within lists, as you might expect. In the box below, turn the latin names for the plants into italics.",
          "question": {
            "input": "* Azalea (Ericaceae Rhododendron)\n* Chrysanthemum (Anthemideae Chrysanthemum)\n* Dahlia (Coreopsideae Dahlia)",
            "result": "* Azalea (*Ericaceae Rhododendron*)\n* Chrysanthemum (*Anthemideae Chrysanthemum*)\n* Dahlia (*Coreopsideae Dahlia*)"
          }
        },
        {
          "title": "Depth lists",
          "body": "Magnificent!\n\nOccasionally, you might find the need to make a list with more depth, or, to *nest* one list within another. Have no fear, because the Markdown syntax is exactly the same. All you have to do is to remember to indent each asterisk *one space more* than the preceding item.\n\nFor example, in the following list, we're going to add some sub-lists to each \"main\" list item, describing the people in detail:\n\n```\n* Tintin\n * A reporter\n * Has poofy orange hair\n * Friends with the world's most awesome dog\n* Haddock\n * A sea captain\n * Has a fantastic beard\n * Loves whiskey\n * Possibly also scotch?\n```\n\nWhen rendered, this list turns into the following grouping:\n\n* Tintin\n  * A reporter\n  * Has poofy orange hair\n  * Friends with the world's most awesome dog\n* Haddock\n  * A sea captain\n  * Has a fantastic beard\n  * Loves whiskey\n  * Possibly also scotch?\n\nIn the box below, turn the character's characteristics into sub-bullets.",
          "question": {
            "input": "* Calculus, A professor, Has no hair, Often wears green\n* Castafiore, An opera singer, Has white hair, Is possibly mentally unwell",
            "result": "* Calculus\n  * A professor\n  * Has no hair\n  * Often wears green\n* Castafiore\n *  An opera singer\n *  Has white hair\n *  Is possibly mentally unwell"
          }
        },
        {
          "title": "Mixing lists with other elements",
          "body": "Stupendous! While you could continue to indent and add sub-lists indefinitely, it's usually a good idea to stop after three levels; otherwise, your text becomes a mess.\n\nThere's one more trick to lists and indentation that we'll explore, and that deals with the case of paragraphs. Suppose you want to create a bullet list that requires some additional context (but not another list). For example, it might look like this:\n\n 1. Crack three eggs over a bowl.\n Now, you're going to want to crack the eggs in such a way that you don't make a mess.\n If you *do* make a mess, use a towel to clean it up!\n\n 2. Pour a gallon of milk into the bowl.\n Basically, take the same guidance as above: don't be messy, but if you are, clean it up!\n\n 3. Rub the salmon vigorously with butter.\n By \"vigorous,\" we mean a strictly vertical motion. Julia Child once quipped:\n    > Up and down and all around, that's how butter on salmon goes.\n\n 4. Drop the salmon into the egg-milk bowl.\n Here are some techniques on salmon-dropping: \n    * Make sure no trout or children are present\n    * Use both hands\n    * Always have a towel nearby in case of messes\n\nTo create this sort of text, your paragraph must start on a line all by itself underneath the bullet point, and it must be indented by at least one space. For example, the list above looks like this in Markdown:\n```\n\n1. Crack three eggs over a bowl.\n Now, you're going to want to crack the eggs in such a way that you don't make a mess.\n If you *do* make a mess, use a towel to clean it up!\n\n 2. Pour a gallon of milk into the bowl.\n Basically, take the same guidance as above: don't be messy, but if you are, clean it up!\n\n 3. Rub the salmon vigorously with butter.\n By \"vigorous,\" we mean a strictly vertical motion. Julia Child once quipped:\n    > Up and down and all around, that's how butter on salmon goes.\n\n 4. Drop the salmon into the egg-milk bowl.\n Here are some techniques on salmon-dropping: \n    * Make sure no trout or children are present\n    * Use both hands\n    * Always have a towel nearby in case of messes\n\n```\n\nNotice that the first two items have a single space. This looks a bit odd, so you might want to indent properly to match the characters up (like items three and four). In these paragraphs, you can include all sorts of other Markdown elements, like blockquotes, or even other lists!\n\nIn the box below, convert the bullet points into their own paragraphs.",
          "question": {
            "input": "1. Cut the cheese\n  * Make sure that the cheese is cut into little triangles.\n\n2. Slice the tomatoes\n  * Be careful when holding the knife.\n  * For more help on tomato slicing, see Thomas Jefferson's seminal essay _Tom Ate Those_.",
            "result": "1. Cut the cheese\n\n   Make sure that the cheese is cut into little triangles.\n\n2. Slice the tomatoes\n\n   Be careful when holding the knife.\n\n   For more help on tomato slicing, see Thomas Jefferson's seminal essay *Tom Ate Those*."
          }
        }
      ],
      "conclusion": "You now know how to make lists in Markdown!"
    },
    {
      "title": "Paragraphs",
      "introduction": "Markdown has several ways of formatting paragraphs.",
      "chapters": [
        {
          "title": "Syntax",
          "body": "Let's take a few lines of poetry as an example. Suppose you want to write text that looks like this:\n\nDo I contradict myself?\nVery well then I contradict myself,\n(I am large, I contain multitudes.)\n\nNow, you might think that simply typing each verse onto its own line would be enough to solve the problem:\n\n```\nDo I contradict myself?\nVery well then I contradict myself,\n(I am large, I contain multitudes.)\n```\nUnfortunately, you'd be wrong! This Markdown would render simply as a single straight line: Do I contradict myself? Very well then I contradict myself, (I am large, I contain multitudes.).\n\nIf you forcefully insert a new line, you end up breaking the togetherness:\n\n```\nDo I contradict myself?\nVery well then I contradict myself,\n(I am large, I contain multitudes.)\n```\n\nThis is what's known as a <em>hard break</em>; what our poetry asks for is a *soft break*. You can accomplish this by inserting two spaces *after* each new line. This is not possible to see, since spaces are invisible, but it'd look something like this:\n\n```\nDo I contradict myself?··\nVery well then I contradict myself,··\n(I am large, I contain multitudes.)\n```\n\nEach dot ( `·` ) represents a space on the keyboard."
        },
        {
          "title": "Hard breaks paragraphs",
          "body": "Let's try this technique out. In the box below, insert the necessary number of spaces to make the poem render correctly:",
          "question": {
            "input": "We pictured the meek mild creatures where\nThey dwelt in their strawy pen,\nNor did it occur to one of us there\nTo doubt they were kneeling then.",
            "result": "We pictured the meek mild creatures where  \nThey dwelt in their strawy pen,  \nNor did it occur to one of us there  \nTo doubt they were kneeling then."
          }
        },
        {
          "title": "Soft breaks paragraphs",
          "body": "Fantastic work! Aside from formatting poetry, one of the common uses for these soft breaks is in formatting paragraphs in lists. Recall in the previous lesson that we inserted a new line for multiple paragraphs within a list.\n\nIn the box below, instead of using hard breaks, tighten the sub-paragraphs with soft breaks:",
          "question": {
            "input": "1. Crack three eggs over a bowl.\n\nNow, you're going to want to crack the eggs in such a way that you don't make a mess.\n\nIf you _do_ make a mess, use a towel to clean it up!\n\n2. Pour a gallon of milk into the bowl.\n\nBasically, take the same guidance as above: don't be messy, but if you are, clean it up!",
            "result": "1. Crack three eggs over a bowl.  \nNow, you're going to want to crack the eggs in such a way that you don't make a mess.  \nIf you _do_ make a mess, use a towel to clean it up!  \n2. Pour a gallon of milk into the bowl.  \nBasically, take the same guidance as above: don't be messy, but if you are, clean it up!"
          }
        }
      ],
      "conclusion": "Et voila! You now know how to make soft breaks in Markdown!"
    }
  ]
}
